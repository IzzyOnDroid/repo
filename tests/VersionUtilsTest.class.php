<?php
require_once(__DIR__.'/../lib/VersionUtils.class.php');

class VersionUtilsTest {
  // List of test cases stored as tuples:
  //  [old version name, new version name, expected result]
  private const TEST_CASES_OLD_NEW_VERSION_NAME = [
    // Version upgrades (within same suffix)
    ["1.0", "1.1", 1],
    ["1.1", "1.0", 0],
    ["1.0.0", "1.0.1", 1],
    ["23.0.1", "22.7.3", 0],
    ["v1.0", "v1.1", 1],
    ["1.0-alpha1", "1.0-alpha2", 1],
    ["1.0-beta1", "1.0-beta2", 1],
    ["1.0-rc1", "1.0-rc2", 1],
    ["1.2.3-beta", "1.2.3-beta2", 1],
    ["1.2.3beta1", "1.2.3beta2", 1],
    ["1.2.3beta-1", "1.2.3beta-2", 1],
    ["1.2.3-beta-1", "1.2.3-beta-2", 1],
    ["1.2.3_beta-1", "1.2.3_beta-2", 1],
    ["1.2.3_beta_1", "1.2.3_beta_2", 1],
    ["1.2.3-beta_1", "1.2.3-beta_2", 1],
    ["1.2.3-beta.1", "1.2.3-beta.2", 1],
    ["0.9.6-pre1", "0.9.6-pre2", 1],
    ["1112", "1113", 1],
    ["004.09.11-16API", "004.09.12-16API", 1],
    ["v.13", "v.14", 1],
    ["1.1.15-tst", "1.2.0-tst", 1],
    ["3.9-prc", "3.9.1-prc", 1],
    ["Build 12", "Build 13", 1],
    ["3.2.57-1", "3.2.58-1", 1],
    ["20220131-1", "20220214-1", 1],
    ["3.0 RC 2", "3.0 RC 3", 1],

    // Version upgrades (with suffix changes)
    ["v0.3.16", "v0.4.0-alpha01", 1],
    ["v0.4.0-alpha10", "v0.4.0-beta01", 1],
    ["v0.4.0-beta10", "v0.4.0-rc01", 1],
    ["v0.4.0-rc03", "v0.4.0", 1],
    ["0.99.0", "1.0.0-alpha1", 1],
    ["0.99.0", "1.0.0-beta1", 1],
    ["0.99.0", "1.0.0-rc1", 1],
    ["1.0", "1.0-alpha1", 0],
    ["1.0-alpha13", "1.0-beta1", 1],
    ["1.0-beta12", "1.0-rc1", 1],
    ["1.0-rc1", "1.0", 1],
    ["1.0-rc3", "1.0", 1],
    ["2.7-beta1", "2.7.1", 1],
    ["2.7-rc7", "2.7-release", 1],
    ["0.6.55-beta", "0.6.57_beta_Carbon", 1],
    ["stable_7", "stable_7_fix_1", 1],
    ["0.9.5.5", "0.9.6-pre1", 1],
    ["0.9.6-pre1", "0.9.6", 1],
    ["3.0 beta 6", "3.0 RC 2", 1],

    // Version upgrades which use single letter track names
    ["1.1", "1.2.a1", 1],
    ["1.2.a1", "1.2.b1", 1],
    ["1.2.b1", "1.2.r1", 1],
    ["1.2.r1", "1.2", 1],
    ["0.22.0_r2", "0.22.0_r3", 1],
    ["0.22.0_r3", "0.22.0", 1],

    // Same versions, should never be considered new
    ["", "", 0],
    ["1.0", "1.0", 0],
    ["1.0", "1.0.0", 0],
    ["1.0.0", "1.0", 0],
    ["1.0.0", "1.0.0", 0],
    ["1.1", "1.01", 0],
    ["1.1", "v1.1", 0],
    ["1.0.0-alpha1", "1.0.0-alpha1", 0],
    ["1.0.0-aLpHa", "1.0.0-Alpha", 0],
    ["1.0.0-beta1", "1.0.0-beta1", 0],
    ["1.0.0-rc1", "1.0.0-rc1", 0],
    ["1.0.0-rc1", "1.0.0-RC1", 0],
    ["1.0.0-rc1", "1.0.0 RC 1", 0],

    // Sanity checks for "strange notations"
    ["0.2.0_r2", "0.2.1", 1],
    ["0.2.0_r2", "0.2.0_r3", 1],
    ["0.2.0", "0.2.0-release", 0],
    ["0.2.0-release", "0.2.0", 0],
    ["0.2.0", "0.2.0-release1", 1],
    ["0.2.0-release", "0.2.1", 1],
  ];

  function testCompareVersionNames() {
    $okCount = 0;
    $failCount = 0;
    foreach (self::TEST_CASES_OLD_NEW_VERSION_NAME as $testCase) {
      list($oldVer, $newVer, $expected) = $testCase;
      $actual = VersionUtils::compareVersionNames($oldVer, $newVer);
      if ($actual == $expected) {
        print("[ OK ] " );
        $okCount++;
      } else {
        print("[FAIL] ");
        $failCount++;
      }
      print("old=" . $oldVer);
      print("  new=" . $newVer);
      print("  actual=" . $actual);
      print("  expected=" . $expected . "\n");
    }
    $sumCount = $okCount + $failCount;
    print("\n" . $okCount . " of " . $sumCount . " test cases passed (" . (($okCount / $sumCount) * 100) . "%)\n");
  }
}

// Directly execute test if in CLI mode
if (PHP_SAPI == "cli") {
  $testInstance = new VersionUtilsTest();
  $testInstance->testCompareVersionNames();
}

?>
