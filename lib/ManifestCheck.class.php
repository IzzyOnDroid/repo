<?php
/** Class to check AndroidManifest from .apk files for security risks
 *  By default, this uses AndroGuard to extract the AndroidManifest. If
 *  'androguard' is not found in the PATH, it falls back to the simpler
 *  AXMLParser provided by AXMLParser.class.php (you can enforce the latter
 *  by calling ManifestCheck::initAXML() explicitly after initializing your
 *  instance of ManifestCheck).
 * @class ManifestCheck
 * @author Izzy (izzysoft AT qumran DOT org)
 * @copyright Andreas Itzchak Rehberg (GPLv2)
 * @brief requires androguard being available in the PATH (fdroidserver requires this as well)
 * @verbatim
 *  Example flow (incomplete):
 *  $check = new ManifestCheck();
 *  // $check->initAXML(); // to ENFORCE the AXMLParser and not use AndroGuard
 *  if ( $check->getManifest($apkfile) ) { // cannot do a thing if AndroidManifest.xml cannot be loaded
 *    $scanResults = $check->parseManifest();
 *  }
 */
class ManifestCheck {
  // ----------------------------------------------[ default configuration ]---
  /** potential risky application properties
   * @property protected array dangerousFlags   application attributes to report
   * @brief 'debuggable' is already checked by fdroidserver so we skip it here
   */
  protected $dangerousFlags  = ['testOnly','usesCleartextTraffic'];
  /** potential risky action intent-filters
   * @property protected array dangerousFilters
   * @verbatim
   *    * android.view.InputMethod should be reserved for keyboard apps (and apps providing one, like Keepass)
   *    * android.net.VpnService should be reserved to VPN apps and network filters like Netguard
   *    * use of android.accessibilityservice.AccessibilityService must be well justified
   */
  protected $dangerousFilters = ['android.accessibilityservice.AccessibilityService','android.net.VpnService','android.view.InputMethod'];
  /** potential risky permissions
   * @property protected array dangerousPermissions
   * @brief these must always match the purpose of the app
   */
  protected $dangerousPermissions = ['ACCESS_BACKGROUND_LOCATION','ACCESS_COARSE_LOCATION','ACCESS_FINE_LOCATION','ACCESS_MEDIA_LOCATION','BLUETOOTH_SCAN','CAMERA','MANAGE_EXTERNAL_STORAGE','NEARBY_WIFI_DEVICES',
      'PROCESS_OUTGOING_CALLS','QUERY_ALL_PACKAGES','READ_CALENDAR','READ_CALL_LOG','READ_CONTACTS','READ_EXTERNAL_STORAGE','READ_MEDIA_AUDIO','READ_MEDIA_IMAGES','READ_MEDIA_VIDEO','READ_PHONE_NUMBERS',
      'READ_PHONE_STATE','READ_SMS','RECEIVE_MMS','RECEIVE_SMS','RECEIVE_WAP_PUSH','RECORD_AUDIO','REQUEST_DELETE_PACKAGES','REQUEST_INSTALL_PACKAGES','SEND_SMS','WRITE_CALL_LOG','WRITE_CONTACTS','SYSTEM_ALERT_WINDOW'];
  /** XML DOMDocument of the AndroidManifest.xml to work on
   * @property protected object manifest
   */
  protected $manifest = NULL;
  /** Mark whether AndroidGuard is available or we have to revert to our own simple parser
   * @property protected mixed androguard
   * @brief this is initial NULL and will be set by the constructor. Either to TRUE when AndroGuard is available, or to an instance of AXMLParser otherwise.
   */
  protected $androguard = NULL;

  // --------------------------------------------------------[ constructor ]---
  /** Constructor
   * @constructor manifestcheck
   */
  public function __construct() {
    if ( trim( shell_exec('which androguard 2> /dev/null') ) ) $this->androguard = true;
    else $this->initAXML();
  }

  // -----------------------------------------------------[ public methods ]---
  /** Initialize AXMLParser
   *  This is used when AndroGuard is not available. It can also be called explicitly if AndroGuard should not be used for some reason.
   * @method initAXML
   */
  public function initAXML() {
    require_once(__DIR__.'/AXMLParser.class.php');
    $this->androguard = new AXMLParser();
  }

  /** Add 'debuggable' to this::dangerousFlags
   *  This is excluded by default as fdroidserver already warns if it's present – but can be useful for pre-scans and use outside fdroidserver environments
   * @method addDebugFlag
   */
  public function addDebugFlag() {
    if ( !in_array('debuggable',$this->dangerousFlags) ) $this->dangerousFlags[] = 'debuggable';
  }

  /** Clear the list of dangerous permissions to set up your own
   * @method clearPerms
   */
  public function clearPerms() {
    $this->dangerousPermissions = [];
  }

  /** Add your own set of what shall be considered "dangerous permissions".
   *  You can pass either a single permission (as string), or an array of permissions (each element being a string).
   * @method addPerms
   * @param mixed perms Permissions to add. Either a string with a single permission, or an array of strings with multiple permissions.
   */
  public function addPerms($perms) {
    if ( is_string($perms) ) {
      if ( !preg_match('![,\s]!',$perms) ) {
        if ( !in_array($perms,$this->dangerousPermissions) ) $this->dangerousPermissions[] = $perms;
      } else trigger_error("'$perms' is not a valid permission. It must not contain commata or white-spaces.",E_USER_WARNING);
    } elseif ( is_array($perms) ) foreach ($perms as $perm) $this->addPerms($perm);
    else trigger_error("'".gettype($perms)."' is not a valid type to pass for \$perms.",E_USER_WARNING);
  }

  /** obtain the plain-text AndroidManifest.xml
   * @method getManifest
   * @param string apkfile location of the APK to analyze
   * @return boolean success
   */
  public function getManifest($apkfile) {
    if ( !file_exists($apkfile) || !is_readable($apkfile) ) {
      trigger_error("getManifest: '$apkfile' does not exist or cannot be read.",E_USER_WARNING);
      return false;
    }
    $manifest = NULL;
    if ( is_object($this->androguard) ) { // fallback if AndroGuard is not available
      if ( $this->androguard->openApk($apkfile) ) {
        $manifest = $this->androguard->getXml();
      } else {
        $this->manifest = NULL;
        $manifest = NULL;
        trigger_error("Could not obtain AndroidManifest.xml from '$apkfile' using AXMLParser.",E_USER_WARNING);
        return false;
      }
    } elseif ( $manifest = '<?xml version="1.0" encoding="utf-8" standalone="no"?>' . trim( shell_exec("androguard --silent axml $apkfile 2>/dev/null") ) ) { // use AndroGuard
      $manifest = preg_replace('#\\x1b[[][^A-Za-z]*[A-Za-z]#', '', $manifest);
    }
    if ( $manifest ) {
      $this->manifest = new DOMDocument();
      if ( !$this->manifest->loadXML($manifest) ) {
        $this->manifest = NULL;
        trigger_error("invalid AndroidManifest.xml for '$apkfile'",E_USER_WARNING);
        return false;
      }
      return true;
    } else {
      $this->manifest = NULL;
      trigger_error("could not load AndroidManifest.xml from '$apkfile'",E_USER_WARNING);
      return false;
    }
  }

  /** Parse the AndroidManifest.xml DOMDocument
   * @method parseManifest
   * @return array results (elements: arrays detectedFlags, detectedFilters, allPermissions (non-associative, just the full list), permissions (pre-23), permissions23 (Sdk23), impliedPermissions (the last 3 associative, with a bool if they are risky)
   */
  public function parseManifest() {
    if ( $this->manifest == NULL ) {
      trigger_error("no AndroidManifest.xml loaded, so nothing to parse. Call getManifest first.",E_USER_WARNING);
    }
    $results = [ 'detectedFlags'=>[], 'detectedFilters'=>[], 'allPermissions'=>[], 'permissions'=>[], 'permissions23'=>[], 'impliedPermissions'=>[] ];
    $targetSdk = 999; // for implied permissions; should we fail to get it, this default value will keep us from implying "older perms"

    // flags and intent filters from <application>
    foreach($this->manifest->getElementsByTagName('application') as $node) { // $node is not a DOMNode, but a DOMElement!
      // check for dangerousFlags
      for($i = $node->attributes->length -1; $i >= 0; $i--) {
        if ( in_array($node->attributes->item($i)->name,$this->dangerousFlags) && $node->attributes->item($i)->value == 'true' ) $results['detectedFlags'][] = $node->attributes->item($i)->name;
        if ( $node->attributes->item($i)->name=='targetSdkVersion' ) $targetSdk = $node->attributes->item($i)->value;
      }
      // children: service with children intent-filter, again with children action that have the filter name in their attributes
      foreach ( $node->childNodes as $child1 ) {
        if ( $child1->nodeName == 'service' ) {
          foreach ( $child1->childNodes as $child2 ) {
            if ( $child2->nodeName == 'intent-filter' ) {
              foreach ( $child2->childNodes as $child3 ) {
                if ( $child3->nodeName == 'action' ) {
                  for($i = $child3->attributes->length -1; $i >= 0; $i--) {
                    if ( in_array($child3->attributes->item($i)->value,$this->dangerousFilters) ) $results['detectedFilters'][] = $child3->attributes->item($i)->value;
                  }
                }
              } // child3
            }
          } // child2
        }
      } // child1
    }

    // permissions
    if ( !empty($this->dangerousPermissions) ) { // could have been emptied via this::clearPerms
      foreach ( $this->dangerousPermissions as $var=>$val ) $dangerousPermissions[$var] = "android.permission.${val}";
      foreach($this->manifest->getElementsByTagName('uses-permission') as $node) {
        for($i = $node->attributes->length -1; $i >= 0; $i--) {
        if ( $node->attributes->item($i)->name != 'name' ) continue;
          $results['allPermissions'][] = $node->attributes->item($i)->value;
          if ( in_array($node->attributes->item($i)->value,$dangerousPermissions) ) {
            $results['permissions'][$node->attributes->item($i)->value] = 1;
          } else {
            $results['permissions'][$node->attributes->item($i)->value] = 0;
          }
        }
      }
      foreach($this->manifest->getElementsByTagName('uses-permission-sdk-23') as $node) {
        for($i = $node->attributes->length -1; $i >= 0; $i--) {
          $results['allPermissions'][] = $node->attributes->item($i)->value;
          if ( in_array($node->attributes->item($i)->value,$dangerousPermissions) ) {
            $results['permissions23'][$node->attributes->item($i)->value] = 1;
          } else {
            $results['permissions23'][$node->attributes->item($i)->value] = 0;
          }
        }
      }
      // implied permissions (see e.g. https://github.com/aosp-mirror/platform_frameworks_base/blob/ec462238f68e4be2baf59b841bc96ba0d2a7fb36/tools/aapt/Command.cpp#L2036)
      if ( $targetSdk < 16 ) {
        if ( $targetSdk < 4) {
          if ( !in_array('android.permission.WRITE_EXTERNAL_STORAGE',$results['allPermissions']) ) {
            $results['allPermissions'][] = 'android.permission.WRITE_EXTERNAL_STORAGE';
            if ( in_array('android.permission.WRITE_EXTERNAL_STORAGE',$dangerousPermissions) ) $results['impliedPermissions']['android.permission.WRITE_EXTERNAL_STORAGE'] = 1;
            else $results['impliedPermissions']['android.permission.WRITE_EXTERNAL_STORAGE'] = 0;
          }
          if ( !in_array('android.permission.READ_PHONE_STATE',$results['allPermissions']) ) {
            $results['allPermissions'][] = 'android.permission.READ_PHONE_STATE';
            if ( in_array('android.permission.READ_PHONE_STATE',$dangerousPermissions) ) $results['impliedPermissions']['android.permission.READ_PHONE_STATE'] = 1;
            else $results['impliedPermissions']['android.permission.READ_PHONE_STATE'] = 0;
          }
        }
        if ( in_array('android.permission.READ_CONTACTS',$results['allPermissions']) && !in_array('android.permission.READ_CALL_LOG',$results['allPermissions']) ) {
          $results['allPermissions'][] = 'android.permission.READ_CALL_LOG';
          if ( in_array('android.permission.READ_CALL_LOG',$dangerousPermissions) ) $results['impliedPermissions']['android.permission.READ_CALL_LOG'] = 1;
          else $results['impliedPermissions']['android.permission.READ_CALL_LOG'] = 0;
        }
        if ( in_array('android.permission.WRITE_CONTACTS',$results['allPermissions']) && !in_array('android.permission.WRITE_CALL_LOG',$results['allPermissions']) ) {
          $results['allPermissions'][] = 'android.permission.WRITE_CALL_LOG';
          if ( in_array('android.permission.WRITE_CALL_LOG',$dangerousPermissions) ) $results['impliedPermissions']['android.permission.WRITE_CALL_LOG'] = 1;
          else $results['impliedPermissions']['android.permission.WRITE_CALL_LOG'] = 0;
        }
      }
      if ( in_array('android.permission.WRITE_EXTERNAL_STORAGE',$results['allPermissions']) && !in_array('android.permission.READ_EXTERNAL_STORAGE',$results['allPermissions']) ) {
        $results['allPermissions'][] = 'android.permission.READ_EXTERNAL_STORAGE';
        if ( in_array('android.permission.READ_EXTERNAL_STORAGE',$dangerousPermissions) ) $results['impliedPermissions']['android.permission.READ_EXTERNAL_STORAGE'] = 1;
        else $results['impliedPermissions']['android.permission.READ_EXTERNAL_STORAGE'] = 0;
      }
    }

    return $results;
  }

}
?>