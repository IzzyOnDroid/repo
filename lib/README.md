This directory holds (some of) the libraries used with the [IzzyOnDroid
Repo](https://apt.izzysoft.de/fdroid). It will be complemented from time to
time, whenever I find the time to do so (mainly some reorganisation and adding
missing documentation to make it understandable for those not having written
the code).

If not specified otherwise, all code is protected by the GPL version 2.0.

## Library Scanner
As I don't compile apps from their resp. sources myself, I want (you to) at
least know what's inside the APKs. For this, the `*.apk` files are scanned
by different means:

* **[VirusTotal](https://virustotal.com/):** multiple malware scanner are run
  against the `*.apk` files there to see if they contain a threat. Results are
  indicated by „shield icons“, and are linked to the full result set at
  VirusTotal. For details on this, [see here](https://apt.izzysoft.de/fdroid/index/info#vt).
  The PHP library used for this can be [found at Codeberg.org](https://codeberg.org/izzy/php-virustotal),
  so I won't duplicate it here.
* **Local Library Scan:** APK files are reverse-engineered using
  [ApkTool](https://github.com/iBotPeaches/Apktool) and their paths are analyzed
  for what libraries have been used. This is a pure static scan comparable to
  the one performed by [Exodus Privacy](https://reports.exodus-privacy.eu.org/);
  it only tells us what libraries are contained in the `*.apk`, but not whether
  they are actually addressed by the app and used at runtime.  
  I've started this around the year 2016 and based it on pkumza's
  [LibRadar](https://github.com/pkumza/LibRadar/tree/V1) (yes, it still uses the
  V1 branch as the more recent master requires quite huge library definitions
  to be downloaded and kept up-to-date). As those library definitions are no
  longer updated, I've complemented them by my own. These you will find here
  along with the code I use to process the APKs.

If you want to use the library scanner stand-alone, take a look at my article
[Identify modules in apps](https://android.izzysoft.de/articles/named/app-modules-2).

### Project making use of these libraries
This list is of course incomplete as I cannot know who pulls them where to, but some places known to me are:

* the [IzzyOnDroid F-Droid repo](https://apt.izzysoft.de/fdroid)
* [F-Droid](https://f-droid.org/) with its [issuebot](https://gitlab.com/fdroid/issuebot/)
* [AppManager](https://github.com/MuntashirAkon/AppManager) with its [libraries](https://github.com/MuntashirAkon/android-libraries)
* [Nostros](https://github.com/KoalaSat/nostros) with [its Github CI workflow](https://github.com/KoalaSat/nostros/blob/main/.github/workflows/android-build.yml?rgh-link-date=2024-04-26T23%3A11%3A59Z#L45)
* [Round-Sync](https://github.com/newhinton/Round-Sync) with [its Github CI workflow](https://github.com/newhinton/Round-Sync/blob/master/.github/workflows/dependencies.yml#L57)


## Manifest Checker
The Manifest Checker scans the `AndroidManifest.xml` contained in the APK files
of each package for indicators to possible risks. It uses [AndroGuard](https://github.com/androguard/androguard)
to obtain the plain-text XML (in the APK files, the file is in binary
[AXML](https://androguard.readthedocs.io/en/latest/intro/axml.html) format). If
the `androguard` binary is not found in the `$PATH`, it falls back to the simpler
AXMLParser provided via `AXMLParser.class.php` in this repo.

Usage of the two is explained inside the PHP files using PHPDoc style comments.
Basicall, you simply run

```php
require('ManifestCheck.class.php');
$check = new ManifestCheck();
// $check->initAXML(); // to ENFORCE the AXMLParser and not use AndroGuard even if available
$check->addDebugFlag(); // excluded by default as fdroidserver already alerts about this
if ( $check->getManifest($apkfile) ) { // if AndroidManifest.xml cannot be loaded there's nothing to scan
  $scanResults = $check->parseManifest();
}
```

Then you can evaluate `$scanResults` for findings. It's an associative array with
the following elements, which are either empty arrays (no findings) or contain
the following:

* `detectedFlags`: if any of the `ManifestCheck::dangerousFlags` have been found, their names
* `detectedFilters`: similarly for `ManifestCheck::dangerousFilters`
* `allPermissions`: full list of permissions declared (non-associative array)
* `permissions`: the "fixed" permissions (pre-Android6 (MarshMallow) style) which are granted automatically
* `permissions23`: runtime permissions (Android 6+)

`permissions` and `permissions23` are associative arrays. An example entry would be:
`scanResults['permissions']['android.permission.QUERY_ALL_PACKAGES'] => 1`
meaning that permission can provide a risk (in this case, the app can obtain a list
of all apps installed on your device – which *can* be fine e.g. for a homescreen
replacement where the app drawer shall show you all apps, but should e.g. not be
requested by a calculator intended to just do, well, calculations.

For details on the `detectedFlags`, please refer to [android:testOnly](https://developer.android.com/guide/topics/manifest/application-element#testOnly),
[android:usesCleartextTraffic](https://developer.android.com/guide/topics/manifest/application-element#usesCleartextTraffic)
and [android:debuggable](https://developer.android.com/guide/topics/manifest/application-element#debug).
The names of the `detectedFilters` speak for themselves and indicate that an app

* provides/integrates a VPN service (redirecting your traffic): `android.net.VpnService`
* provides/integrates a keyboard/input service: `android.view.InputMethod`
* or makes use of Android's accessibility features: `android.accessibilityservice.AccessibilityService`


## Files
### LibRadar
* `libradar.class.php`: the PHP wrapper around LibRadar and its complements.
  The code is documented using „PHPDoc“, including short instructions on its usage.
* `libradar.jsonl`, `libradar_wild.jsonl`: files complementing LibRadar's own library
  definitions. Structure etc. is explained inside `libradar.class.php`.
* `libsmali.jsonl`: my own library definitions, run against the Smali code left by
  LibRadar when it has extracted it via ApkTool. This is done after the results
  from LibRadar have been processed, and merged with those.


### My own library definitions
These are also shared with *Exodus Privacy* whenever I've found something not yet
available with their definitions. But they hold not only tracking stuff, but also
„general development libraries“ (and frameworks) like Flutter, Material Dialogs,
Color Pickers etc.

* `libsmali.jsonl`: see above.
* `libinfo.jsonl`: informational details to show to the user, like descriptions of
  the libraries, but also AntiFeatures triggered by a library (e.g. Ads, non-free
  dependencies).
* `libs_verificator.php`: a PHP script to verify the aforementioned two definition
  files are syntactically correct and have all mandatory fields set.

For an easy „stand-alone“ setup of my library scanner (and some background details),
see e.g. my article [Identify modules in apps](https://android.izzysoft.de/articles/named/app-modules-2).


### the Apk-Checker
* `apkcheck_db_struct.sql`: database structure. I use SQLite for this (easy sync
  from the "job machine" to the "web server"), but these could be adopted for
  other DBs without much hazzle
* `apkcheck_db.class.php`: class dealing with the database. The required
  SQLite3 base class (or the one for MySQL/MariaDB if you prefer) can be obtained
  e.g. from [phpVideoPro](https://github.com/IzzySoft/phpVideoPro)
* `apkcheck.class.php`: the wrapper around VirusTotal and libradar classes
  dealing with checking `*.apk` files for malware and libraries. This is the
  only one directly addressed for this process.
* `apkscan.ini`: configuration
* `../bin/apkScan.php`: the main script – that's the one to run!
* `../bin/checkAntis.php`: cross-check each app's latest package against our
  scanner result library for possibly missing AntiFeatures (e.g. dragged in by
  a new library dependency)
* `../bin/cleanIcons`: script to remove icons of deleted apps (fdroidserver simply
  ignores them)
* `../bin/getInfected.sql`: controls what findings are reported when running `apkScan.php`
* `../bin/getInfectedChecker`: checks the exception rules in `getInfected.sql` and
  automatically removes lines for APKs that no longer exist
* `../bin/getRelease.php`: the core CLI script to check for updates and retrieve them
* `../bin/locallibs.php`: cross-check Smali from apps against our library definitions
  for new candidates
* `../bin/rescan.php`: to rescan an APK file (eg. after library definitions were
  updated) and/or schedule a rescan with VirusTotal
* `../bin/scanapk.php`: to manually check an APK file for libraries. Will output a list
  of libraries detected, as well as a second list of libraries having AntiFeatures (if
  such were found in the app)

### the Manifest Checker
* `ManifestCheck.class.php`: the Manifest Checker itself. Requires either AndroGuard or
* `AXMLParser.class.php`: fallback for `AndroidManifest.xml` extraction and decoding if AndroGuard is not available or shall not be used for some reason

### Other files
* `ApkUpdater.class.inc`: the core library taking care to check for and pull
  updated APKs from various sources, extending `upstream.class.php`
* `antifeatures.json`: descriptions of the AntiFeatures you might already know from
  [F-Droid](https://f-droid.org/). This is used [on the website](https://apt.izzysoft.de/fdroid)
  with the app descriptions.
* `antifeatures_spans.json`: corresponding JSON for the symbols indicating
  AntiFeatures in lists (HTML SPANs)
* `class.logging.php`: simple logging class used by CLI executions e.g. of the Apk-Checker
* `upstream.class.php`: dealing with „upstream APIs“, like Github/GitLab/Gitea, to
  obtain information on Releases, Files etc. Also uses `apkscan.ini` for configuration
  (see Apk-Checker above).
* `yaml/`: YAML parser from the Symfony project. This intentionally is an older, light-weight
  version not drawing in thousands of dependencies, and used to parse metadata files of
  our apps (for things not contained in the F-Droid index, especially the specific details
  maintained in `MaintainerNotes`)


### Files I won't publish here
… because they can be found in their own repos:

* `fdroid.class.php`: the class I wrote to get app details from F-Droid's XML and
  JSON indexes can [be found here](https://gitlab.com/fdroid/php-fdroid)
* `virustotal.class.php`: the class I wrote to have VirosTotal scan the APK files.
   You can [find it at Codeberg.Org](https://codeberg.org/izzy/php-virustotal)
* [LibRadar](https://github.com/pkumza/LibRadar/tree/V1) & [ApkTool](https://github.com/iBotPeaches/Apktool)
* Michel Fortin's [PHP Markdown Extra](https://michelf.ca/projects/php-markdown/extra/) (used for cleanup/formatting of some Fastlane `full_description.txt`)
