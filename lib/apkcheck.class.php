<?php
require_once(__DIR__.'/apkcheck_db.class.php');
require_once(__DIR__.'/libradar.class.php');
require_once(__DIR__.'/virustotal.class.php');

/** Class to check .apk files for libraries and malware
 * @class apkcheck
 * @author Izzy (izzysoft AT qumran DOT org)
 * @copyright Andreas Itzchak Rehberg (GPLv2)
 * @verbatim
 *  Example flow (incomplete):
 *  $libsfile = 'libradar.jsonl'; $libsWildFile = 'libradar_wild.jsonl'; $apiKey = '';
 *  $radarDir = '/path/to/LibRadar/main';
 *  $apkcheck = new apkcheck('/path/to/fdroid/repo','apkcheck.db');
 *  $apkcheck->initRadar($radarDir,$libsFile='',$libsWildFile='');
 *  $apkcheck->initVT($apiKey);
 *  $apkcheck->newFile('com.example.app','com.example.app_7.apk','https://github.com/example/app/blob/master/example.apk?raw=true');
 */
class apkcheck {

  /** Path to our repo (where the .apk files to process are located)
   * @class apkcheck
   * @attribute protected str repoPath
   */
  protected $repoPath = '';

  /** Our database sub-class
   * @class apkcheck
   * @attribute protected object db
   */
  protected $db;

  /** Our LibRadar sub-class
   * @class apkcheck
   * @attribute protected object libradar
   */
  protected $libradar = NULL;

  /** Shall libRadar report unknowns?
   * @class apkcheck
   * @attribute protected str libRadarUnknowns
   * @info valid settings for now: 'none' (do not report), 'echo' (report to STDOUT)
   */
  protected $libRadarUnknowns = 'none';

  /** Our VirusTotal sub-class
   * @class apkcheck
   * @attribute protected object virusTotal
   */
  public $virusTotal = NULL;

// ========================================================[ Initialization ]===
  /** Initialize the class
   *  This just sets up the database backend. LibRadar and VirusTotal need to be
   *  initialized separately with the corresponding methods (initRadar and initVT)
   * @constructor apkcheck
   * @param string repoPath Path to our repo (where the .apk files to process are located)
   * @param string dbfile   SQLite database file to use (must exist and be accessible r/w of course)
   */
  public function __construct($repoPath,$dbfile) {
    $this->repoPath = $repoPath;
    if ( empty($dbfile) || !file_exists($dbfile) || !is_writeable($dbfile) ) {
      trigger_error("The database file specified ($dbfile) cannot be found or is write-protected.",E_USER_ERROR);
    }
    $this->db = new apkcheck_db($dbfile);
  }

  /** Initialize the LibRadar sub-system
   * @class apkcheck
   * @method initRadar
   * @param str     radarDir        directory where LibRadar's main.py can be found
   * @param opt str libsFile        location of additional library definitions (default: libradar.jsonl in current dir)
   * @param opt str libsWildFile    location of additional library definitions by "wildcard" (default: libradar_wild in current dir)
   * @param opt str libsSmaliFile   location of additional library definitions for our own Smali scan
   * @param opt str libRadarUnknowns what to do when encounter an unidentified lib: 'none' (nothing, default), 'echo' (to STDOUT)
   */
  public function initRadar($radarDir,$libsFile='',$libsWildFile='',$libsSmaliFile,$libRadarUnknowns='none') {
    if ( !$this->libradar = new libradar($radarDir,$libsFile,$libsWildFile,$libsSmaliFile) ) $this->libradar = NULL; // set to NULL if unusable
    if ( in_array($libRadarUnknowns,['none','echo']) ) $this->libRadarUnknowns = $libRadarUnknowns;
  }

  /** Initialize the VirusTotal sub-system
   * @class apkcheck
   * @method initVT
   * @param string api_key VirusTotal API key
   */
  public function initVT($api_key) {
    if ( empty($api_key) ) {
      trigger_error("VirusTotal requires an API key. An empty string won't do.",E_USER_ERROR);
      $this->virusTotal = NULL;
    } else {
      $this->virusTotal = new virustotal($api_key);
    }
  }

// ====================================================[ main functionality ]===
  /** Get details on a file
   * @class apkcheck
   * @method getFile
   * @param str file    name of the file
   * @return array      fileInfo: strings local_file_name, github_file_url, pkgname, vt_permalink; JSON vt_result, apk_libraries, apk_paymodules, apk_admodules; ints vt_detected, libcount
   */
  public function getFile($file) {
    return $this->db->getFile($file);
  }

  /** List all file names we have recorded
   * @class apkcheck
   * @method listFileNames
   * @param opt bool infectedOnly=FALSE
   * @return array of str fileNames
   */
  public function listFileNames($infectedOnly=FALSE) {
    return $this->db->listFileNames($infectedOnly);
  }

  /** New files are added to the DB queue for VirusTotal and processed by LibRadar
   * @class apkcheck
   * @method newFile
   * @param str pkgname     package name of the corresponding app
   * @param str filename    name of the file to enqueue
   * @param opt str url     corresponding (Github/GitLab/Codeberg/other_source) URL the APK was obtained from
   * @return bool success
   */
  public function newFile($pkgname,$filename,$url='') {
    $res = $this->getFile($filename);
    $this->db->add2q($pkgname,$filename,$url); // enqueue for VirusTotal
    if ( $this->libradar != NULL && empty($res) ) $this->radarFile($filename); // only scan if it wasn't there before
  }

  /** Remove an entry from the Q (usually once it was successfully processed)
   * @class apkcheck
   * @method delFromQ
   * @param str file    name of the file
   * @return bool success
   */
  function delFromQ($file) {
    return $this->db->delFromQ($file);
  }

  /** Get queued files
   * @class apkcheck
   * @method getQ
   * @param opt str qtype Type of enqueued files: ALL (default), IDLE (just pushed but not yet processed), WAITING (uploaded, no result yet), READY (have results), NOTREADY (all except READY)
   * @return array queueItems 0..n of [local_file_name,vt_json,vt_hash,vt_response_code,entry_time]
   */
  public function getQ($qtype='ALL') {
    return $this->db->getQ($qtype);
  }

  /** Let LibRadar process a file and update our database correspondingly
   * @class apkcheck
   * @method radarFile
   * @param str filename    name of the file to enqueue
   * @return bool success
   */
  public function radarFile($filename) {
    $file = $filename;
    if ( !empty($this->repoPath) ) $filename = $this->repoPath.'/'.$filename;
    $res = $this->libradar->scan($filename,$this->libRadarUnknowns);
    $libs = [];
    $pays = [];
    $ads  = [];
    $ma   = [];
    $libcount = 0;
    foreach($res as $item) {
      switch($item['type']) {
        case "Payment"      : $pays[] = $item; break;
        case "Advertisement": $ads[]  = $item; break;
        case "Mobile Analytics" : $ma[] = $item; break;
        default             : $libs[] = $item; break;
      }
      ++$libcount;
    }
    $info = $this->db->getFile($file);
    return $this->db->updateFile($file,$info['github_file_url'],$info['pkgname'],
                                 $info['vt_permalink'],$info['vt_result'],$info['vt_detected'],
                                 json_encode($libs),json_encode($pays),json_encode($ads),json_encode($ma),
                                 $libcount
                                );
  }

  /** Check for VirusTotal details on a file
   * @class apkcheck
   * @method vtFile
   * @param str filename        name of the file to enqueue
   * @param opt str hash        hash of the file (if known)
   * @return number haveResults -9x: error on our end; -1: error from VT; 0: no results, 1: results ready; use self::virusTotal::getResponse() to obtain details
   */
  public function vtFile($filename,$hash='') {
    $file = $filename;
    if ( !empty($this->repoPath) ) $filename = $this->repoPath.'/'.$filename;
    $rc   = $this->virusTotal->checkFile($filename,$hash);
    $res  = $this->virusTotal->getResponse();
    $reso = json_decode($res);
    switch($rc) {
      case  0:  // queued, no results yet
            ( property_exists($reso,'sha256') ) ? $sha = $reso->sha256 : $sha = $hash;
            $rd = $this->db->updateQ($file,$res,$sha,0);
            if ($rd) return 0;
            else return -90;
            break;
      case  1:  // have results
            $info = $this->db->getFile($file);
            $rd = $this->db->updateFile($file,$info['github_file_url'],$info['pkgname'],
                                        $reso->permalink,$res,$reso->positives,
                                        $info['apk_libraries'],$info['apk_paymodules'],$info['apk_admodules'],
                                        $info['apk_analyticsmodules'],$info['libcount']
                                       );
            if ($rd) $rd = $this->delFromQ($file); // successfully processed
            if ($rd) return 1;
            else return -91;
            break;
      case -1:  // error
      default:
            return $rc;
            break;
    }
  }

  /** Schedule a rescan with VirusTotal
   * @class apkcheck
   * @method vtRescan
   * @param str filename        name of the file to enqueue
   * @param opt str hash        File Hash (MD5/SHA256) of the file to rescan
   * @param opt str maxage      max age (in days, default:7) for an already existing result set. If it's newer, we won't ask for a rescan but stick with that. Set to 0 to enforce a rescan.
   * @return number haveResults -99: got no response; -95: no hash given and file not found to create one;
   *                            -1: error; 0: file is enqueued, 1: results ready; use self::virusTotal::getResponse() to obtain details;
   *                            other negative values: other errors (most likely unknown / not described in API and should not happen)
   */
  function vtRescan($filename,$hash='',$maxage=7) {
    $file = $filename;
    if ( empty($hash) ) {
      if ( !empty($this->repoPath) ) $filename = $this->repoPath.'/'.$filename;
      if ( !file_exists($filename) ) return -95;
      $hash = hash_file('sha256', $filename);
    }
    $rc = $this->virusTotal->rescan($hash,$maxage);
    $res  = $this->virusTotal->getResponse();
    $reso = (object) json_decode($res);
    switch ( $rc ) { // do we need to update our database?
      case  1: // we already have results
        $info = $this->db->getFile($file);
        $rd = $this->db->updateFile($file,$info['github_file_url'],$info['pkgname'],
                                    $reso->permalink,$res,$reso->positives,
                                    $info['apk_libraries'],$info['apk_paymodules'],$info['apk_admodules'],
                                    $info['apk_analyticsmodules'],$info['libcount']
                                   );
        if ($rd) $rd = $this->delFromQ($file); // successfully processed
        if ($rd) return $rc;
        else return -91;
      case  0: // just enqueued
        ( property_exists($reso,'sha256') ) ? $sha = $reso->sha256 : $sha = $hash;
        $rd = $this->db->updateQ($file,$res,$sha,0);
        if ($rd) return $rc;
        else return -90;
        break;
      default: return $rc; break;
    }
  }

  /** Delete a file from our records
   * @class apkcheck_db
   * @method updateFile
   * @param str file            name of the file
   * @return bool success
   */
  public function deleteFile($file) {
    return $this->db->deleteFile($file);
  }

  /** Compress database
   * @class apkcheck_db
   * @method compressDB
   */
  function compressDB() {
    $this->db->compress();
  }

}
?>