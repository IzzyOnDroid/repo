-- .read apkcheck.sql
-- sqlite3 apkcheck.db < apkcheck.sql

CREATE TABLE files (            -- library details for our APKs
  local_file_name VARCHAR,      -- file name as stored in our repo
  github_file_url VARCHAR,      -- URL of the corresponding file at Github
  pkgname VARCHAR,              -- package name of the corresponding app
  vt_permalink VARCHAR,         -- URL to VirusTotal details on the file, if any
  vt_result TEXT,               -- JSON with scan results (or empty)
  vt_detected INT,              -- how many alerts have been detected by VirusTotal (check vt_result for details)
  apk_libraries TEXT,           -- JSON with details on detected libraries other than pay,ad,analytics (or empty)
  apk_paymodules TEXT,          -- JSON with details on detected payment services modules
  apk_admodules TEXT,           -- JSON with details on detected ad modules (or empty)
  apk_analyticsmodules TEXT,    -- JSON with details on detected analytics modules (or empty)
  libcount INT,                 -- number of libraries altogether
  CONSTRAINT pk_files PRIMARY KEY(local_file_name)
);

CREATE TABLE job_queue (        -- job queue for tasks pending with VirusTotal
  local_file_name VARCHAR,      -- file name as stored in our repo
  vt_json TEXT,                 -- JSON response from submit
  vt_hash VARCHAR,              -- MD5/SHA256 hash identifying the resource
  vt_response_code INT,         -- 0:not present, 1:ready for retrieval (results embedded in JSON), -2:still enqueued
  entry_time INT                -- time this file entered the queue (Unix time as returned by PHP's time() or SQLite's strftime('%s','now'))
  --CONSTRAINT fk_jobqueue_files FOREIGN KEY (local_file_name) REFERENCES files(local_file_name) ON DELETE CASCADE ON UPDATE CASCADE
);

-- ON DELETE CASCADE doesn't seem to work with SQLite, so we do that via a trigger:
CREATE TRIGGER tr_del_abandoned_from_queue
  BEFORE DELETE ON files
  FOR EACH ROW BEGIN
    DELETE FROM job_queue WHERE local_file_name = old.local_file_name;
  END;

CREATE VIEW app_states AS
  SELECT pkgname,                               -- package name of the app
         COUNT(local_file_name) AS filecount,   -- number of APK files
         SUM(vt_detected) AS vt_detected,       -- number of threats detected by VirusTotal (sum for all APKs of the app)
         SUM(libcount) AS libcount              -- number of libraries (sum for all APKs of the app)
    FROM files
   GROUP BY pkgname
   ORDER BY pkgname;
