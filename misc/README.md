This directory holds some miscellaneous files from the [IzzyOnDroid
Repo](https://apt.izzysoft.de/fdroid). For further details, please see
[../libs/README.md](../libs/README.md).

If not specified otherwise, all code is protected by the GPL version 2.0.


### Files needed for setup
* `apkcheck.sql`: to create the database for the library checker
* `main.py`: replacement for the `<libradar>/main/main.py` with a small adjustment to provide the extracted Smali information for post-processing
