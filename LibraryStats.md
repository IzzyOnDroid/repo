# Library statistics
These are statistics on the library details collected so far. This does not only give you an idea of what details are known – but also about details still missing. Which is where you can help: filling the gaps! So if you know any of those details, please report them (with a reference to confirm them).

## General library stats
* number of libraries recorded: 3450
* number of library types: 11
* number of AntiFeatures: 8

## Library types
| Library type | # libs |
| :----------- | -----: |
| Utility | 2200 |
| UI Component | 811 |
| Mobile Analytics | 130 |
| Development Aid | 107 |
| Development Framework | 74 |
| Advertisement | 53 |
| Map | 32 |
| Payment | 21 |
| Social Network | 10 |
| App Market | 6 |
| Game Engine | 5 |

## AntiFeatures
| AntiFeature | # libs |
| :---------- | -----: |
| Tracking | 161 |
| NonFreeComp | 131 |
| NonFreeNet | 130 |
| Ads | 58 |
| NonFreeDep | 12 |
| NonFreeAdd | 10 |
| NonFreeAssets | 6 |
| SelfUpdater | 3 |

## License Statistics
* Libraries without licenses: 84
* Libraries with no known license: 170

Top-5 most used licenses:

| License | # libs |
| :------ | ---: |
| Apache-2.0 | 1736 |
| MIT | 858 |
| BSD-3-Clause | 131 |
| Proprietary | 103 |
| GPL-3.0-only | 88 |

## Libraries with missing details
* libraries with missing description: 24
* libraries with missing/no license: 170 / 84

### Missing descriptions
* [AndroidX Inspection](https://github.com/androidx/androidx/tree/androidx-main/inspection) (`/androidx/inspection`)
* [Arcs](https://github.com/PolymerLabs/arcs) (`/arcs`)
* [calculator2](https://android.googlesource.com/platform/packages/apps/Calculator/+/froyo/src/com/android/calculator2) (`/com/android/calculator2`)
* [Android Internal APIs](https://android.googlesource.com/platform/frameworks/base/+/HEAD/core/java/com/android/internal/) (`/com/android/internal`)
* [android-kxml2](https://android.googlesource.com/platform/libcore/+/8cfc479/xml/src/main/java/org/kxml2) (`/com/android/org/kxml2`)
* [Smart-Show](https://github.com/vincent-series/smart-show) (`/com/coder/vincent/smart_show`)
* [Callbacks](https://github.com/DylanCaiCoding/Callbacks) (`/com/dylanc/callbacks`)
* [DrawerOverlayService](https://github.com/FabianTerhorst/DrawerOverlayService) (`/com/google/android/apps/gsa/nowoverlayservice`)
* [Android SetupCompat](https://android.googlesource.com/platform/external/setupcompat/) (`/com/google/android/setupcompat`)
* [Android SetupDesign](https://android.googlesource.com/platform/external/setupdesign/+/refs/heads/main) (`/com/google/android/setupdesign`)
* [Waxmoon OpenSDK](https://github.com/WaxMoon/opensdk) (`/com/hack/opensdk`)
* [Telemetry-Client-for-Android](https://github.com/Microsoft/Telemetry-Client-for-Android) (`/com/microsoft/cll/android`)
* [RxUtils](https://github.com/Tans5/RxUtils) (`/com/tans/rxutils`)
* [Wutka DTD](https://mvnrepository.com/artifact/com.liferay/com.wutka.dtd) (`/com/wutka/dtd`)
* [Themable Widget Library](https://github.com/MatthiasMann/twl) (`/de/matthiasmann/twl`)
* [Coinswap](https://github.com/irismod/coinswap) (`/irismod/coinswap`)
* [SmartStudy](https://www.smartstudy.co.kr/en/) (`/kr/co/smartstudy`)
* [HawtBuf](https://mvnrepository.com/artifact/org.fusesource.hawtbuf/hawtbuf) (`/org/fusesource/hawtbuf`)
* [Java Arithmetics Engine (formerly Arity)](https://github.com/hoijui/arity) (`/org/javia/arity`)
* [AbstractTask](https://github.com/mariotaku/AbstractTask) (`/org/mariotaku/abstask`)
* [S4M](http://www.s4m.io/) (`/S4M`)
* [Tealium](https://tealium.com/) (`/tealium`)
* [CollapsiblePreferenceCategory](https://github.com/zacharee/CollapsiblePreferenceCategory) (`/tk/zwander/collapsiblepreferencecategory`)
* [PatreonSupportersRetrieval](https://github.com/zacharee/PatreonSupportersRetrieval) (`/tk/zwander/patreonsupportersretrieval`)

### License unknown
* [AdsWizz](http://www.adswizz.com/) (`/adswizz`)
* [Appvisor push](https://www.app-visor.com/) (`/biz/appvisor/push/android/sdk`)
* [AdColony](http://adcolony.com/) (`/com/adcolony`)
* [AdForm](https://site.adform.com/) (`/com/adform/sdk`)
* [Demdex](https://www.adobe.com/data-analytics-cloud/audience-manager.html) (`/com/adobe/mobile/Analytics`)
* [AMap](https://lbs.amap.com/api/android-location-sdk/locationsummary) (`/com/amap/api`)
* [Amazon Analytics](https://developer.amazon.com/public/apis/manage/analytics/docs/analytics-for-android-fire-os) (`/com/amazon/insights`)
* [IVS Player](https://docs.aws.amazon.com/ivs/) (`/com/amazonaws/ivs`)
* [Android Instant Run](https://developer.android.com/studio/run/index.html#instant-run) (`/com/android/tools/ir`)
* [Google Play](https://play.google.com/) (`/com/android/vending`)
* [Appsee](https://www.appsee.com/) (`/com/appsee`)
* [Areametrics](https://areametrics.com/) (`/com/areametrics/areametricssdk`)
* [Avocarrot](https://www.avocarrot.com/) (`/com/avocarrot/sdk`)
* [Baidu Maps](https://map.baidu.com) (`/com/baidu/BaiduMap`)
* Baidu Location Service (`/com/baidu/location`)
* [Baidu Mobile Ads](https://developer.baidu.com/) (`/com/baidu/mobads`)
* [Baidu Mobile Stat](https://developer.baidu.com/) (`/com/baidu/mobstat`)
* [Baidu Navigation](http://lbsyun.baidu.com/index.php?title=android-navsdk) (`/com/baidu/navi`)
* [Beaglebuddy mp3 java library](http://beaglebuddy.com/) (`/com/beaglebuddy`)
* [BlueConic](https://www.blueconic.com/) (`/com/blueconic`)
* [BlueKai (acquired by Oracle)](http://bluekai.com/registry/) (`/com/bluekai/sdk`)
* [BugClipper](https://bugclipper.com/sdk-doc/android/) (`/com/bugclipper/android`)
* [Cuebiq](http://www.cuebiq.com/) (`/com/cuebiq/cuebiqsdk`)
* [Teemo](https://www.teemo.co/) (`/com/databerries`)
* [DMM GAMES ASDK](https://dmmgames.co.jp/) (`/com/dmm/asdk/api`)
* [Estimote](https://estimote.com/) (`/com/estimote`)
* [ExactTarget](http://help.exacttarget.com/en/technical_library/API_Overview/) (`/com/exacttarget`)
* [AccountKit](https://www.accountkit.com/) (`/com/facebook/accountkit`)
* Facebook Ads (`/com/facebook/ads`)
* [Facebook Audience](https://developers.facebook.com/docs/android/) (`/com/facebook/audiencenetwork`)
* [Facebook Unity SDK](https://developers.facebook.com/docs/unity/) (`/com/facebook/unity`)
* [FidZup](https://www.fidzup.com/) (`/com/fidzup`)
* [Fiksu](https://fiksu.com/) (`/com/fiksu/asotracking`)
* [Flurry Ads](http://www.flurry.com/) (`/com/flurry/android/ads`)
* [Flurry Analytics](http://www.flurry.com/) (`/com/flurry`)
* [ForeSee](https://www.foresee.com/) (`/com/foresee/sdk/ForeSee`)
* [Fyber](https://www.fyber.com/) (`/com/fyber`)
* [GameAnalytics](https://gameanalytics.com/docs/item/android-sdk) (`/com/gameanalytics`)
* [Gigya](https://www.gigya.com/) (`/com/gigya`)
* Android Market (`/com/google/android/finsky`)
* [Google Android Net](http://developer.android.com/reference/android/net/package-summary.html) (`/com/google/android/net`)
* [Google ARCode](https://developers.google.com/ar/develop/) (`/com/google/ar/core`)
* [Cloud Audit Logs](https://cloud.google.com/logging/docs/audit) (`/com/google/cloud/audit`)
* [Firebase](https://firebase.google.com/) (`/com/google/firebase`)
* [Play Games Services](https://developers.google.com/games/services/android/quickstart) (`/com/google/games`)
* [Heyzap (bought by Fyber)](https://www.heyzap.com/) (`/com/heyzap`)
* [Huawei Appmarket](https://developer.huawei.com/consumer/en/) (`/com/huawei/appmarket/component`)
* Huawei HMF Tasks (`/com/huawei/hmf/tasks`)
* [HMS Signin](https://developer.huawei.com/consumer/en/) (`/com/huawei/hms/hwid/internal`)
* [Huawei Update SDK](https://developer.huawei.com/consumer/en/) (`/com/huawei/updatesdk`)
* [ICNA nProtect GameGuard](https://inca.co.kr/en/index.html) (`/com/inca/security`)
* [inMobi](http://www.inmobi.com/) (`/com/inmobi/ads`)
* [Inrix](http://inrix.com/) (`/com/inrix/sdk`)
* [BlueCove](http://bluecove.org/) (`/com/intel/bluetooth`)
* [Add Apt Tr](https://www.addapptr.com) (`/com/intentsoftware/addapptr`)
* [ironSource](https://www.ironsrc.com/) (`/com/ironsource`)
* [AdFit (Daum)](https://www.daum.net/) (`/com/kakao/adfit/ads`)
* [Kontakt](https://kontakt.io/) (`/com/kontakt/sdk/android`)
* [Krux](https://www.salesforce.com/products/marketing-cloud/data-management) (`/com/krux/androidsdk`)
* [Lisnr](http://lisnr.com/) (`/com/lisnr`)
* [Localytics](http://localytics.com/) (`/com/localytics/android`)
* [Locuslabs](http://locuslabs.com/) (`/com/locuslabs/sdk`)
* [Mobile Engagement](https://docs.microsoft.com/en-us/azure/mobile-engagement/mobile-engagement-android-sdk-overview) (`/com/microsoft/azure/engagement`)
* [Millennial Media](http://www.millennialmedia.com/) (`/com/millennialmedia`)
* [LEDataStream](http://mindprod.com/products1.html#LEDATASTREAM) (`/com/mindprod/ledatastream`)
* [MAdvertise](http://madvertise.com/) (`/com/mngads`)
* [Moat Analytics](https://moat.com/analytics) (`/com/moat/analytics`)
* [MoMo](https://developers.momo.vn/#/) (`/com/momo/sdk`)
* [mTraction](https://www.mtraction.com/) (`/com/mtraction/mtractioninapptracker`)
* MySQL JDBC (`/com/mysql/jdbc`)
* [NativeX](http://www.nativex.com/) (`/com/nativex`)
* [Nexage](http://nexage.com/) (`/com/nexage/android`)
* [Omniture](http://www.omniture.com/) (`/com/omniture`)
* [OutBrain](https://www.outbrain.com/) (`/com/outbrain`)
* [Pulsate](https://www.pulsatehq.com/) (`/com/pulsatehq`)
* [Pushbullet API](https://docs.pushbullet.com/) (`/com/pushbullet/android`)
* Tencent Social Ads (`/com/qq/e/ads`)
* [QuickBlox](https://quickblox.com/) (`/com/quickblox`)
* [Radius Networks](https://www.radiusnetworks.com/) (`/com/radiusnetworks`)
* [Retency](http://retency.com/) (`/com/retency/sdk/android`)
* [Rubikon Project](https://rubiconproject.com/) (`/com/rfm/sdk`)
* [Safe Graph](https://www.safegraph.com/) (`/com/safegraph`)
* [Sailthru](https://docs.mobile.sailthru.com/docs/sdk-integration) (`/com/sailthru/mobile/sdk`)
* [Samsung Mobile SDK Pass (Fingerprint)](http://developer.samsung.com/onlinedocs/sms/pass/com/samsung/android/sdk/pass/package-summary.html) (`/com/samsung/android/sdk/pass`)
* [Samsung Accessory SDK](https://developer.samsung.com/galaxy-accessory/overview.html) (`/com/samsung/android/sdk`)
* [Samsung Overlay](http://developer.samsung.com/java/technical-docs/Samsung-Overlay-Keypad-Overlay-EditField) (`/com/samsung/util`)
* [SAS SDK](https://communities.sas.com/t5/SAS-Communities-Library/Building-a-SAS-CI-enabled-mobile-app-for-Android/ta-p/354922) (`/com/sas/mkt/mobile/sdk`)
* [Scandit](https://scandit.com/) (`/com/scandit`)
* [Sense360](https://sense360.com/) (`/com/sense360/android/quinoa/lib/Sense360`)
* [Sensoro](https://www.sensoro.com/) (`/com/sensoro/beacon/kit`)
* [Shopkick](https://shopkick.com/) (`/com/shopkick/sdk/api`)
* [Signal360](http://www.signal360.com/) (`/com/signal360/sdk/core`)
* [SilverPush](http://silverpush.co/) (`/com/silverpush`)
* [Weibo](http://weibo.com/) (`/com/sina/weibo/sdk`)
* [Smart](http://smartadserver.com) (`/com/smartadserver`)
* [Snap Kit SDK](https://kit.snapchat.com/) (`/com/snapchat/kit/sdk`)
* [Snowplow](https://snowplowanalytics.com/) (`/com/snowplowanalytics`)
* [Splunk MINT](http://docs.splunk.com/Documentation/MintAndroidSDK) (`/com/splunk/mint`)
* [Fyber SponsorPay](http://www.sponsorpay.com/) (`/com/sponsorpay`)
* [Singlespot](https://www.singlespot.com/) (`/com/sptproximitykit`)
* [Supersonic Ads](https://www.supersonic.com/) (`/com/supersonic/adapters/supersonicads`)
* [TeleQuid](http://www.telequid.com/) (`/com/telequid`)
* [Teliver](https://www.teliver.io/) (`/com/teliver/sdk`)
* [Tencent Map LBS](https://lbs.qq.com/) (`/com/tencent/lbs`)
* TencentSearch (`/com/tencent/lbssearch`)
* [Tencent Wechat](https://open.weixin.qq.com/) (`/com/tencent/mm`)
* [Tencent MobWin](https://www.tencent.com/en-us/) (`/com/tencent/mobwin`)
* [Tencent MTA](https://mta.qq.com/) (`/com/tencent/mta`)
* [Tencent Stats](http://stat.qq.com/) (`/com/tencent/stat`)
* [AndroidUtils](https://github.com/Tianscar/AndroidUtils) (`/com/tianscar/androidutils`)
* [Tinder Ads](https://www.tinder.com/) (`/com/tinder/ads`)
* [Tinder Analytics](https://www.tinder.com/) (`/com/tinder/analytics`)
* [Tune](https://www.tune.com/) (`/com/tune`)
* [Uber Analytics](https://uber.com/) (`/com/ubercab/analytics`)
* [Unity Mobile Notifications](https://docs.unity3d.com/Packages/com.unity.mobile.notifications@1.0) (`/com/unity/androidnotifications`)
* [Unity IAP](https://docs.unity3d.com/ja/2019.4/Manual/com.unity.purchasing.html) (`/com/unity/purchasing`)
* [Unity3d Ads](https://unity3d.com/) (`/com/unity3d/ads`)
* [Upsight](https://help.upsight.com/api_sdk_reference/android/#sdk-setup) (`/com/upsight`)
* [UXCam](https://help.uxcam.com/hc/en-us/articles/115000966252-Android-Integration) (`/com/uxcam/UXCam`)
* [Syc2Ad](https://www.sync.tv/) (`/com/visiware/sync2ad/dmp`)
* [Loggly](http://loggly.com/) (`/com/visiware/sync2ad/logger/loggly`)
* [Weborama](http://www.weborama.com/) (`/com/weborama`)
* [Webtrends](https://www.webtrends.com/) (`/com/webtrends/mobile/analytics`)
* [Xiaomi Push](http://mipush.global.xiaomi.com/) (`/com/xiaomi/mipush`)
* [TransCommu Android SDK](http://transcommu.yasesprox.com/transcommu/Help/AndroidSdkHelp) (`/com/yasesprox/android/transcommusdk`)
* [YotaDevices SDK](https://yotaphone.com/ae-en/developer/docs/develop/building-first-app-epd/) (`/com/yotadevices/sdk`)
* [Zebra API](http://techdocs.zebra.com/link-os/latest/webservices/content/overview-summary.html) (`/com/zebra/sdk`)
* [Zenjoy](http://www.zenjoy.net/) (`/com/zenjoy/ads`)
* [DexClassLoader](https://developer.android.com/reference/dalvik/system/package-summary) (`/dalvik/system/DexClassLoader`)
* [AutoScale TextView](http://ankri.de/autoscale-textview/) (`/de/ankri/views`)
* [AppAnalytics](http://appanalytics.io/) (`/io/appanalytics/sdk`)
* [AppLink.io](https://applink.io/) (`/io/applink/applinkio/AppLinkIO`)
* [Ogury Presage](http://www.presage.io/) (`/io/presage`)
* [Repro](https://docs.repro.io/en/dev/sdk/getstarted/android.html) (`/io/repro/android/Repro`)
* [RongCloud](https://www.rongcloud.cn/) (`/io/rong/imlib`)
* [Vectaury](https://www.vectaury.io/) (`/io/vectaury`)
* [Java Streams](https://developer.android.com/reference/java/util/stream/package-summary) (`/java/util/stream`)
* [JavaX MID Profile](http://docs.oracle.com/javame/config/cldc/ref-impl/midp2.0/jsr118/) (`/javax/microedition`)
* [JavaX Bluetooth OBEX](https://docs.oracle.com/javame/config/cldc/opt-pkgs/api/bluetooth/jsr082/javax/obex/package-summary.html) (`/javax/obex`)
* [JavaX WebServices RESTful Resources](https://docs.oracle.com/javaee/7/api/javax/ws/rs/package-summary.html) (`/javax/ws/rs`)
* [Caulis FraudAlert](https://caulis.jp/en/) (`/jp/caulis/fraud/sdk`)
* [Polarify](https://www.polarify.co.jp/) (`/jp/co/polarify`)
* [LINE SDK](https://developers.line.biz/ja/docs/android-sdk/) (`/jp/line/android/sdk`)
* [Adfurikun](https://adfurikun.jp/adfurikun/) (`/jp/tjkapp/adfurikunsdk`)
* [SmartStudy](https://www.smartstudy.co.kr/en/) (`/kr/co/smartstudy`)
* [Ligatus](http://ligatus.com/) (`/LigatusManager`)
* [Microsoft MapPoint](http://www.microsoft.com/mappoint/) (`/microsoft/mappoint`)
* [Colocator](https://developers.colocator.net/) (`/net/crowdconnected/androidcolocator`)
* [Tasker Open Icon Pack](http://ipack.dinglisch.net/) (`/net/dinglisch/ipack`)
* [sanukin](https://github.com/sanukin39) (`/net/sanukin`)
* [Sesame Shortcuts](https://sesame.ninja/) (`/ninja/sesame`)
* [AChartEngine](http://www.achartengine.org/) (`/org/achartengine`)
* [Java-1-Class-Utilities](https://github.com/andresoviedo/java-utilities) (`/org/andresoviedo/util`)
* [AOP Alliance](https://sourceforge.net/projects/aopalliance/) (`/org/aopalliance`)
* [Astian Libs](https://gitlab.astian.org/apps-mobiles/astian-libs) (`/org/astiansuite/libs`)
* [Cocos2d-x](https://www.cocos2d-x.org/) (`/org/cocos2dx`)
* [FMOD](https://fmod.com/) (`/org/fmod`)
* [Secure Remote Password Protocol](https://docs.jboss.org/jbosssecurity/docs/6.0/security_guide/html/chap-Secure_Remote_Password_Protocol.html) (`/org/jboss/security/srp`)
* [Ministro Qt shared libraries](https://community.kde.org/Necessitas) (`/org/kde/necessitas/ministro`)
* [Lantern MobileSDK](https://github.com/getlantern/lantern) (`/org/lantern/mobilesdk`)
* [Mozilla Charset Detector](https://www-archive.mozilla.org/projects/intl/universalcharsetdetection) (`/org/mozilla/intl/chardet`)
* [Mantissa](https://spaceroots.org/mantissa-doc/overview-summary.html) (`/org/spaceroots/mantissa`)
* [WalletConnect](https://docs.walletconnect.org/) (`/org/walletconnect`)
* [libwebrtc-android](https://github.com/signalapp/libwebrtc-android) (`/org/whispersystems/libwebrtc_android`)
* [JStribog](https://github.com/javabeanz/jstribog) (`/ru/ubmb/jstribog`)
* [S4M](http://www.s4m.io/) (`/S4M`)
* [Sizmek](https://www.sizmek.com) (`/sizmek`)
* [Tealium](https://tealium.com/) (`/tealium`)
* [Alphonso](http://alphonso.tv/) (`/tv/alphonso/service`)
* [Westhawk's Java SNMP Stack](https://snmp.westhawk.co.uk/api/) (`/uk/co/westhawk/snmp`)

