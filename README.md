[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](https://codeberg.org/IzzyOnDroid/.profile/src/branch/master/CODE_OF_CONDUCT.md)

This project is about the F-Droid compatible repo running at https://apt.izzysoft.de/fdroid/

The main purpose of this project is to enable you to actively contribute to the repo (e.g. by suggesting apps that should be added or removed), as well as to reach out for help. Suggestions for other improvals are of course welcomed, too.

Further, most of the scripts and libraries used at *IzzyOnDroid* are provided here using FOSS licenses. You can find those (with some documentation) in the [`bin/`](bin/) and [`lib/`](lib/) directories and use them according to their resp. licenses.


## What is the IzzyOnDroid Repo?
It is an F-Droid style repository for Android apps, provided by [IzzyOnDroid]. Applications in this repository are official binaries built and signed by the original
application developers, taken from their resp. repositories (mostly Github). You can find a more detailed description [here][1].


## What is the purpose behind it?
Many users are not happy with their devices' ties to Google, so they try to cut them back as far as possible. Not being bound to Google's Play Store is part of that – but the choice of apps is quite restricted outside Playstore
if you don't want to replace one „evil“ with another (i.e. switching to another „walled garden“ like the Amazon Store) and still have the comfort of updates. The *IzzyOnDroid* repo wants to increase the number of apps available to this group.


## Why another repository – hasn't F-Droid its own?
Indeed it has, and a good one at that. But they also have some [inclusion criteria][2] many apps cannot meet, or are not yet ready to meet (most common examples include „blobs“ or dependencies on proprietary frameworks).
That's where the *IzzyOnDroid* repo jumps in: Most of those apps are acceptable here, as the requirements are different. So while e.g. preparing to meet criteria from „over there“, an app can be (temporarily or even permanently)
hosted here. Other apps also have their permanent place at *IzzyOnDroid* because they won't ever get rid of those dependencies – for example if those are a central part of the app's functionality – or their authors simply feel more home here.


## How can I get my favorite app listed there?
This is what this project here at GitLab was set up for: use the issues to propose new apps. Of course, first make sure the app you wish to add isn't already there. Then, also check it meets the requirements in our [[Inclusion Policy]].


## What are the requirements an app must meet to be included with the repo?
This section has been moved to the [[Inclusion Policy]] wiki page.


## Are there any app categories which are not acceptable to this repo?
This section has been moved to the [[Inclusion Policy]] wiki page.


## If my app is available here, do you have a badge I can use to link to it?
Yes, indeed we have – for the purpose of linking to our repo, you can use [this PNG](assets/IzzyOnDroid.png), or one of the other graphics available in the [`assets/` directory](assets/) (see there for details on usage and credits):

<center><img src="assets/IzzyOnDroid.png" width="170"></center>

You can also use Shields.io to show which version of your app is available at IzzyOnDroid. Details on this can be found [in the wiki](https://gitlab.com/IzzyOnDroid/repo/-/wikis/API). This is how it would look like:

<center><img src="https://img.shields.io/endpoint?url=https://apt.izzysoft.de/fdroid/api/v1/shield/com.mirfatif.permissionmanagerx&label=IzzyOnDroid&cacheSeconds=86400"></center>


## Why is $someApp only available as $someArch?
Some apps are only available with e.g. their `arm64` or `armeabi` variants, though their developers also serve other architectures. This is due to size limitations (see inclusion criteria above) and happens when the „fat build“ (i.e. the one holding multiple architectures in a single APK) would not fit the per-app size limit. Usually, `armeabi` is chosen here, as the repo strives to help keeping older devices useful as long as possible (keyword: sustainability) and most 64bit devices still support 32bit APKs (exceptions are still very few) – while the opposite can not be said (a 32bit device naturally cannot support 64bit APKs). In few cases, the `arm64` is here instead – usually if there are enough other alternatives available.

If you think the other arch of an app should be here instead of the one that is here, please clarify that with the app developers first. We usually follow their decision on this, and won't switch architectures without their agreement. And before you ask: architectures other than `armeabi` and `arm64` won't be supported here except as part of a „fat build“ including ARM as well.


## Are apps removed from the repo – and when does that happen?
This indeed is the case, and usually happens when the inclusion criteria (see above) are no longer met. So this is especially the case when an app…

* can no longer count as „free/libre“ (e.g. the license changed, or too many non-free dependencies have been added)
* started downloading additional binaries without the explicit and informed consent of the user (e.g. by integrating a self-updater that is not strictly opt-in and/or does not clearly outline its implications)
* is reported (with evidence) for malicious behavior
* is no longer maintained and reported to be no longer working
* exceeds the alotted space with no means of remediation (a possible remediation often is switching to a per-ABI build, see above „`$somearch`“)

Note that becoming available in another F-Droid Repo (e.g. at [F-Droid]) no longer means it will be „automatically removed“ here. If the size of its APK files stays well inside the limits outlined above, it will usually be kept.


## I'm just a simple, not tech-savy user, how can I support you?
True, there's much time we spend on my open source projects, and server costs have to be considered as well. So if you're just a happy user looking how you can give some „backing“ – you're very welcome to
support us at our [OpenCollective](https://opencollective.com/izzyondroid), or send some mBTC to the address [1K7i1VJYjRgjdVzaMfK2XRxLsyhjSFXPnC](bitcoin:1K7i1VJYjRgjdVzaMfK2XRxLsyhjSFXPnC).
For other alternatives like SEPA payment, please find detailed [explanations at IzzyOnDroid][3].

Apart from that, support is always welcomed with

* improving app descriptions
* providing missing screenshots
* reporting missing/wrong URLs (e.g. if an app's repo moved, or a website/changelog was added)
* finding and reporting good apps matching above criteria so they can be added
* reporting „violating” apps as well as such that no longer work so they can be removed
* filling [gaps in the library details](LibraryStats.md)

If you’re going on a hunt to report a bulk, please open one issue per app so it will be easier to process. But even if you by accident stumble on a little thing to be improved, your report will be welcome!


[IzzyOnDroid]: https://android.izzysoft.de/
[Github]: https://github.com/
[GitLab]: https://gitlab.com/
[Codeberg]: https://codeberg.org/
[Sourceforge]: https://sourceforge.net/
[F-Droid]: https://f-droid.org/
[1]: https://apt.izzysoft.de/fdroid/index/info
[2]: https://f-droid.org/wiki/page/Inclusion_Policy "F-Droid Inclusion Policy"
[3]: https://android.izzysoft.de/help?topic=support_us "Say thanks to IzzyOnDroid"
